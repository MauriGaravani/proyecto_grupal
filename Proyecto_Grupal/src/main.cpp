#include <Arduino.h> //Incluimos las librerias 
#include <Keypad.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Interrupt_isr.h>

void setup() {
  pinMode(ENTRADA_PIR, INPUT); //Declaramos la entrada y salida de pines
  pinMode(LEDVERDE, OUTPUT);
  pinMode(LEDROJO, OUTPUT);
  pinMode(VALVULA, OUTPUT);
  pinMode(CONTADOR, OUTPUT); 
  digitalWrite(CONTADOR, HIGH);  
  control_led (LED_APAGADO, LED_ENCENDIDO);
  lcd.init(); //Declaramos la pantalla lcd
  Serial.begin(9600); //Comunicacion serie iniciada a 9600 baudios
  attachInterrupt(digitalPinToInterrupt(ENTRADA_PIR), interrupcion_pir, CHANGE); //Interrupcion 
}

void loop() {
  char tecla = kpd.getKey(); // Definimos la variable donde se almacena el valor obtenido
  //         TECLA 1         //  
  if (tecla == '1')
  {
    if(tecla1 == false)
    {
      control_led (LED_ENCENDIDO, LED_APAGADO);
      lcd.backlight();
      lcd.setCursor(0, 0);
      lcd.print("Bienvenidos al  ");
      lcd.setCursor(0, 1);
      lcd.print("programa        ");   
      delay(1500);
      lcd.setCursor(0, 0);
      lcd.print("1-Apagar 2-ModoA");
      lcd.setCursor(0, 1);
      lcd.print("3-Tiempo #-Menu ");   
      tecla1 = true;  
    }
    else
    {
      control_led (LED_APAGADO, LED_ENCENDIDO);
      lcd.clear();
      lcd.noBacklight();
      tecla1 = false;
    }
  }
  //         TECLA 2         //
  if (tecla == '2')
  {
    if(tecla2 == false)
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Modo Ahorro     ");
      lcd.setCursor(0, 1);
      lcd.print("Iniciado        ");  
      tiempo = 500;
      delay(1500);
      lcd.setCursor(0, 0);
      lcd.print("2-Modo Normal   ");
      lcd.setCursor(0, 1);
      lcd.print("#-Volver al menu"); 
      tecla2 = true;
    }
    else
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Modo Normal     ");
      lcd.setCursor(0, 1);
      lcd.print("Iniciado        ");  
      tiempo = 2000;
      delay(1500);
      lcd.setCursor(0, 0);
      lcd.print("2-Modo Ahorro   ");
      lcd.setCursor(0, 1);
      lcd.print("#-Volver al menu"); 
      tecla2 = false;
    }
  }
  //         TECLA 3         //
  if (tecla == '3')
  {
    if(tecla3 == false)
    { 
      tecla3 = true;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Contador tiempo ");
      lcd.setCursor(0, 1);
      lcd.print("Activado        ");
      delay(1500);
      lcd.setCursor(0, 0);
      lcd.print("3-Desactivar    ");
      lcd.setCursor(0, 1);
      lcd.print("#-Volver al menu"); 
    }
    else
    {
      tecla3 = false;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Contador tiempo ");
      lcd.setCursor(0, 1);
      lcd.print("Desactivado     ");
      delay(1500);
      lcd.setCursor(0, 0);
      lcd.print("3-Activar Tiempo");
      lcd.setCursor(0, 1);
      lcd.print("#-Volver al menu");      
    }
  }
  //         TECLA #         //
  if (tecla == '#')
  {
    lcd.setCursor(0, 0);
    lcd.print("1-Apagar 2-ModoA");
    lcd.setCursor(0, 1);
    lcd.print("3-Tiempo #-Menu ");
  }
  //         Funcionamiento         //
  if (tecla1 == true) digitalWrite(VALVULA, estado);  
  if ((tecla3 == true) && (estado == false)) digitalWrite(CONTADOR, LOW);
  if (estado == true) digitalWrite(CONTADOR, HIGH);
}