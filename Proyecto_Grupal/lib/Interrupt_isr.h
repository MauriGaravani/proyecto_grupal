#include <Arduino.h> //Librerias 
#include <Keypad.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#define ENTRADA_PIR 2 //Macros
#define VALVULA 3
#define CONTADOR 9
#define LEDVERDE 10
#define LEDROJO 11
#define LED_ENCENDIDO HIGH
#define LED_APAGADO LOW

bool tecla1 = false, tecla2 = false, tecla3 = false, estado = true; //Declaramos las variables
int tiempo = 2000; 

void interrupcion_pir (void); //Prototipados
void control_led (bool, bool);

LiquidCrystal_I2C lcd(0x3F,16,2); //Crea un objeto de la clase LiquidCrystal_I2C

const byte ROWS = 2; //Numero de filas
const byte COLS = 3; //Numero de columnas 
char keys[ROWS][COLS] = {
	{'1','2','3'},
	{'*','0','#'}
};
byte rowPins[ROWS] = {4, 5}; //Pines de las filas
byte colPins[COLS] = {6, 7, 8}; //Pines de las columnas

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );